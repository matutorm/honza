import SwaggerClient from 'swagger-client';

export function apiClient()  {
  return new SwaggerClient({
    url: 'http://petstore.swagger.io/v2/swagger.json',
  });
}